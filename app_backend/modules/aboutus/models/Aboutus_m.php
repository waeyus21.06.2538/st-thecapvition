<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Aboutus_m extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_data($type=null)
    {
        if (isset($type))
            $this->db->where('type', $type);
        $query = $this->db
                        ->from('table_all')
                        ->get();
        return $query;
    }
    
    public function update($value, $type)
    {
        $this->db
                ->where('type', $type)
                ->delete('table_all');
        $rs = $this->db
                    ->insert('table_all', $value);
        return $rs;
    }
}
