<div class="col-md-12">
    <div class="m-portlet m-portlet--tab">
        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                       แบบฟอร์ม
                    </h3>
                </div>
            </div>
            <div class="m-portlet__head-tools">
                
            </div>
        </div>
        <?php echo form_open($frmAction, array('class' => 'm-form m-form--fit m-form--label-align-right form-horizontal  frm-main', 'method' => 'post')) ?>
        <div class="m-portlet__body">
             <div class="form-group m-form__group row m-form__group row">
                <label class="col-sm-2 col-form-label" > <h4 class="block">พื้นฐาน</h4></label>
                <div class="col-sm-7"></div>
            </div>  
           
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label" >ชื่อเว็บไซต์ (Title)</label>
                <div class="col-sm-8">
                    <input value="<?php echo isset($info['siteTitle']) ? $info['siteTitle'] : NULL ?>" type="text" id="" class="form-control" name="siteTitle">
                </div>
            </div>  
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">คำอธิบายเว็บไชต์ (Description)</label>
                <div class="col-sm-8">
                    <textarea class="form-control" rows="5"  class="form-control" name="metaDescription"><?php echo isset($info['metaDescription']) ? $info['metaDescription'] : NULL ?></textarea>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-sm-2 col-form-label">คำหลักเว็บไซต์ (Keyword)</label>
                <div class="col-sm-8">
                    <textarea class="form-control" rows="3"  class="form-control" name="metaKeyword"><?php echo isset($info['metaKeyword']) ? $info['metaKeyword'] : NULL ?></textarea>
                </div>
            </div>                    
        </div>
        <div class="m-portlet__foot m-portlet__foot--fit">

            <div class="m-form__actions">
                <div class="row">
                    <div class="col-2">
                    </div>
                    <div class="col-10">
                        <button type="submit" class="btn btn-primary pullleft">บันทึก</button>
                <!-- <button type="reset" class="btn btn-secondary">Cancel</button> -->
                    </div>
                </div>
                
            </div>
        </div>
        
        <?php echo form_close() ?>

    </div>  
</div>


