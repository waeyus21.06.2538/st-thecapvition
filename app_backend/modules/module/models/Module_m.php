<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Module_m Extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }
    
    public function get_rows($param) {
        $this->_condition($param);
        
        if ( isset($param['length']) ) 
            $this->db->limit($param['length'], $param['start']);
        
        $this->db->order_by('order', 'asc')
                ->order_by('parentId', 'asc');        
        
        $query = $this->db
                        ->select('a.*')
                        ->from('module a')
                        ->get();
        return $query;
    }
    
    public function get_count($param){
        $this->_condition($param);
        $query = $this->db
                        ->select('a.*')
                        ->from('module a')
                        ->get();
        return $query->num_rows();
    }
    
    private function _condition($param){
//        if ( isset($param['search']['value'])) {
//            $this->db
//                    ->group_start()
//                    ->like('a.title', $param['search']['value'])
//                    ->group_end();
//        }

//        if ( isset($param['order']) ){
//            if ($param['order'][0]['column'] == 1) $columnOrder = "a.title";
//            $this->db
//                    ->order_by($columnOrder, $param['order'][0]['dir']);
//        }
        
        if ( isset($param['email']) ) 
            $this->db->where('a.email', $param['email']);
        
        if ( isset($param['moduleId']) ) 
            $this->db->where('a.moduleId', $param['moduleId']);
        
        if ( isset($param['recycle']) )
            $this->db->where('a.recycle', $param['recycle']);
        
        if ( $this->router->method == 'order' ) {
            $this->db->where('active', 1);
        }
    }
    
    public function get_by_id($id)
    {
        $query = $this->db
                        ->select('a.*')
                        ->from('module a')
                        ->where('a.moduleId', $id)
                        ->get()
                        ->row_array();
        return $query; 
    }
    
    public function row_by_class($class) {
        
        $query = $this->db
                        ->select('*')
                        ->from('module a')
                        ->where('class', $class)
                        ->where('active', 1)
                        ->get();
        return $query->row();
    }    
    
    public function get_menu()
    {
        $this->db
                ->order_by('parentId', 'asc')
                ->order_by('order', 'asc')
                ->order_by('moduleId', 'asc'); 
        $query = $this->db
                        ->from('module a')
                        ->select('a.moduleId, a.title, a.parentId, a.order')
                        ->where('a.isSidebar', 1)
                        ->where('a.recycle', 0)
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function get_menu_order()
    {
        $query = $this->db
                        ->from('module a')
                        ->select('a.moduleId, a.title, a.parentId, a.order')
                        ->where('a.isSidebar', 1)
                        ->where('a.status', 1)
                        ->where('a.recycle', 0)
                        ->order_by('a.order')
                        ->order_by('a.parentId')
                        ->get()
                        ->result_array();
        return $query;
    }
    
    public function insert($value)
    {
        $this->db->insert('module', $value);
        return $this->db->insert_id();
    }
    
    public function delete($param)
    {
        $rs = $this->db
                    ->where_in('moduleId', $param)
                    ->delete('module');
        return $rs;
    }
    
    public function update($value, $id)
    {
        $rs = $this->db
                    ->where('moduleId', $id)
                    ->update('module', $value);
        return $rs;        
    }  
    
    public function update_order($value)
    {
        $rs = $this->db
                    ->update_batch('module', $value, 'moduleId');
        return $rs;   
    }
    
    public function update_in($id, $value)
    {
        $query = $this->db
                        ->where_in('moduleId', $id)
                        ->update('module', $value);
        return $query;
    }

    public function get_max_id()
    {
        $query = $this->db
                        ->select('moduleId')
                        ->from('module')
                        ->order_by('moduleId','DESC')
                        ->limit(1)
                        ->get()
                        ->row_array();
        return $query['moduleId']+1; 
    }
    
}

