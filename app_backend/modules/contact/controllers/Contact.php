<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'third_party/vendor/autoload.php';
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Contact extends MX_Controller {

    private $_title = "จัดการข้อมูลติดต่อเรา";
    private $_pageExcerpt = "การจัดการข้อมูลเกี่ยวกับ จัดการข้อมูลติดต่อเรา";
    private $_grpContent = "contact";
    private $_requiredExport = true;
    private $_permission;

    public function __construct(){
        parent::__construct();
        $this->_permission = Modules::run('permission/check');
        if ( !$this->_permission && !$this->input->is_ajax_request() ) {
            Modules::run('utils/toastr','error', config_item('appName'), 'ขอภัยคุณไม่ได้รับสิทธิการใช้นี้');
            redirect_back();
        }
        $this->load->library('ckeditor');
        $this->load->model("contact_m");
        $this->load->library('uploadfile_library');
        $this->load->model("aboutus/aboutus_m");
    }
    
    public function index(){
        $this->load->module('template');

        $type = 'contact';
        $info = $this->aboutus_m->get_data($type);
        $data['info'] = $info->row();
        $data['frmAction'] = site_url("{$this->router->class}/update/{$type}");
        
        // breadcrumb
        $data['breadcrumb'][] = array($this->_title, site_url("{$this->router->class}"));
        // $data['breadcrumb'][] = array('แก้ไข', site_url("{$this->router->class}/edit"));
        
        // page detail
        $data['pageHeader'] = $this->_title;
        $data['pageExcerpt'] = $this->_pageExcerpt;
        $data['contentView'] = "{$this->router->class}/form";
        
        $this->template->layout($data);
    }    
    
    public function update($type){
        $input = $this->input->post(null, true);
        $input['type'] = $type;
        $value = $this->_build_data($input);
        
        $result = $this->aboutus_m->update($value, $type);
        if ( $result ) {
            Modules::run('utils/toastr','success', config_item('appName'), 'บันทึกรายการเรียบร้อย');
        } else {
            Modules::run('utils/toastr','error', config_item('appName'), 'บันทึกรายการไม่สำเร็จ');
        }
        redirect(site_url("{$this->router->class}"));
    }
    
    private function _build_data($input){
        $value = array();
        
        $value['title'] = $input['title'];
        $value['slug'] = $input['slug'];
        $value['excerpt'] = html_escape($this->input->post('excerpt', FALSE));
        // $value['excerpt'] = $input['excerpt'];
        $value['type'] = $input['type'];

        if($input['metaTitle']!=""){
            $value['metaTitle'] = $input['metaTitle'];
        }else{
            $value['metaTitle'] = $input['title'];
        }
        if($input['metaDescription']!=""){
            $value['metaDescription'] = $input['metaDescription'];
        }else{
            $value['metaDescription'] = '';
        }
        if($input['metaKeyword']!=""){
            $value['metaKeyword'] = $input['metaKeyword'];
        }else{
            $value['metaKeyword'] = $input['title'];
        }

        $value['updated_at'] = db_datetime_now();
        $value['updated_by'] = $this->session->users['user_id'];
        
        return $value;
    }

    public function sumernote_img_upload(){
		//sumernote/img-upload
		$path = 'contact';
        $upload = $this->uploadfile_library->do_upload('file',TRUE,$path);
        
		if(isset($upload['index'])){
			$picture = $this->config->item('root_url').'uploads/'.$path.'/'.date('Y').'/'.date('m').'/'.$upload['index']['file_name'];
		}

		echo $picture;
    }

    public function deletefile($ids){

		$arrayName = array('file' => $ids);

		echo json_encode($arrayName);
	}
}
