<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['courses/ajax_load_courses'] = 'courses/ajax_load_courses';
$route['courses/learning'] = 'courses/learning';
$route['courses/(:any)'] = 'courses/detail/$1';
$route['courses-lesson/(:any)'] = 'courses_lesson/detail/$1';
$route['courses-lesson/(:any)/(:num)'] = 'courses_lesson/detail/$1/$2';

$route['exams/index-pretest/(:any)/(:num)'] = 'exams/index_pretest/$1/$2';
$route['exams/exam-pretest/(:any)/(:num)'] = 'exams/exam_pretest/$1/$2';
$route['exams/index-posttest/(:any)/(:num)'] = 'exams/index_posttest/$1/$2';
$route['exams/exam-posttest/(:any)/(:num)'] = 'exams/exam_posttest/$1/$2';
$route['exercise/exer-exercise/(:num)'] = 'exercise/exercise_detail/$1';
$route['exercise/exercise-history/(:num)'] = 'exercise/exercise_history/$1';
$route['exercise/exer-exercise/(:any)/(:num)'] = 'exercise/exercise_detail/$1/$2';

$route['exams-pretest-posttest/(:any)'] = 'exams_manage_pretest_posttest/$1';
$route['exams-pretest-posttest/(:any)/(:num)'] = 'exams_manage_pretest_posttest/$1/$2';
$route['exams-pretest-posttest/(:any)/(:num)/(:num)'] = 'exams_manage_pretest_posttest/$1/$2/$3';

$route['exams-pretest-posttest-list/(:any)'] = 'exams_manage_pretest_posttest_list/$1';
$route['exams-pretest-posttest-list/(:any)/(:num)'] = 'exams_manage_pretest_posttest_list/$1/$2';
$route['exams-pretest-posttest-list/(:any)/(:num)/(:num)'] = 'exams_manage_pretest_posttest_list/$1/$2/$3';

$route['instructors/courses/edit/(:any)'] = 'instructors/edit_courses/$1';
$route['profile/my-course'] = 'profile/mycourses';
$route['form-register'] = 'login/formRegister';
$route['my-profile'] = 'login/profile';
$route['logout'] = 'login/logout';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

// $route['page/(:any)'] = 'aboutus/index/$1';
