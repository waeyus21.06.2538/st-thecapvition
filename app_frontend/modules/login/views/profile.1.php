
<div class="container">
    <div class="background-image">
        <div class="row">
            <div class="offset-sm-2 col-sm-8">
                <div class="my-5">
                    <div class="">
                        <h2 class="title text-center">ข้อมูลส่วนตัว</h2>
                        <form id="EditForm" class="form-signin" action="<?=site_url('login/update');?>" method="post" enctype="multipart/form-data">
                            <div class="form-label-group">
                            <label for="name">รหัสสมาชิก</label>
                            <input type="text" id="couponCode" name="couponCode" class="form-control " readonly="" value="<?php echo isset($Users->couponCode) ? $Users->couponCode : NULL ?>">
                        </div>
                        <div class="form-label-group">
                            <label for="name">ชื่อ</label>
                            <input type="text" id="fname" name="fname" class="form-control " placeholder="ชื่อ" value="<?php echo isset($Users->fname) ? $Users->fname : NULL ?>">
                        </div>
                        <div class="form-label-group">
                            <label for="name">นามสกุล</label>
                            <input type="text" id="lname" name="lname" class="form-control" placeholder="นามสกุล" value="<?php echo isset($Users->lname) ? $Users->lname : NULL ?>">
                        </div>
                        <div class="form-label-group">
                             <label for="phone">เบอร์โทร</label>
                            <input type="text" id="phone" name="phone" value="<?php echo isset($Users->phone) ? $Users->phone : NULL ?>" class="form-control" placeholder="เบอร์โทร">
                           
                        </div>
                        <div class="form-label-group">
                             <label for="email">Email</label>
                            <input type="email" id="email" name="email" value="<?php echo isset($Users->email) ? $Users->email : NULL ?>" class="form-control" placeholder="Email" <?php if ($Users->email) {echo 'disabled';} ?>>
                           
                        </div>
                        <div class="form-label-group">
                            <label for="oldPassword">รหัสผ่านปัจจุบัน</label>
                            <input type="password" id="oldPassword" name="oldPassword" class="form-control"
                                placeholder="รหัสผ่าน">
                            
                        </div>
                        <div class="form-label-group">
                             <label for="password">รหัสผ่านใหม่</label>
                            <input type="password" id="password" name="password" class="form-control"
                                placeholder="รหัสผ่าน">
                           
                        </div>
                        <div class="form-label-group">
                            <label for="confirm_password">ยืนยันรหัสผ่านใหม่</label>
                            <input type="password" id="confirm_password" name="confirm_password" class="form-control"
                                placeholder="ยืนยันรหัสผ่าน">
                            
                        </div>
                            <input type="hidden" name="input-mode" id="input-mode" value="edit">
                            <input type="hidden" name="id" id="input-id" value="<?php echo isset($Users->user_id) ? encode_id($Users->user_id) : 0 ?>">
                            <button class="btn btn-lg btn-primary btn-block text-uppercase mt-3" type="submit">บันทึก</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>