  <!-- Full Page Intro -->
  <?php  echo Modules::run('banners/about',''); ?>
  <!-- Full Page Intro -->

  <!--Main layout-->
  <main>
    <div class="container">
      <!--Section: Main info-->
      <section class="mt-5 wow fadeIn">
        <div class="row">
          <div class="col">
            <?=!empty($info->excerpt)? html_entity_decode($info->excerpt) : ''?>
          </div>
      </section>
      <!--Section: Main info-->

    </div>
  </main>
  <!--Main layout-->