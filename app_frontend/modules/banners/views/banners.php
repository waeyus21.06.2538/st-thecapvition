<?php 
    if(!empty($info)):
?>
<div class="page-header header-filter" data-parallax="true" style="background-image: url(<?=base_url($info[0]->file)?>)">
    <div class="container">
      <div class="row">
        <div class="col-md-8 ml-auto mr-auto">
          <div class="brand text-center">
            <!-- <h1>Your title here</h1>
            <h3 class="title text-center">Subtitle</h3> -->
          </div>
        </div>
      </div>
    </div>
  </div>
<?php 
  endif;
?>    