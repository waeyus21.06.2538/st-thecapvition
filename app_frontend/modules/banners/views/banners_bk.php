
<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
  <ol class="carousel-indicators">
    <?php 
      if(!empty($info)):
          foreach($info as  $key=>$item):
    ?>
      <li data-target="#carouselExampleIndicators" data-slide-to="<?=$key?>" class="<?=$key == 0? 'active' : '' ?>"></li>
      <!-- <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
      <li data-target="#carouselExampleIndicators" data-slide-to="2"></li> -->
    <?php 
        endforeach;
      endif;
    ?>
  </ol>
  <div class="carousel-inner size-banner">
  <?php 
    if(!empty($info)):
        foreach($info as  $key2=>$item2):
  ?>
    <div class="carousel-item <?=$key2 == 0? 'active' : '' ?>">
      <img class="w-100 pc" src="<?=base_url($item2->file)?>" alt="<?=$item2->title? $item2->title : ''?>" onerror="this.src='<?=base_url('template/frontend/img/card.png');?>'">
      <img class="w-100 mobile" src="<?=base_url($item2->file_mb)?>" alt="<?=$item2->title? $item2->title : ''?>" onerror="this.src='<?=base_url('template/frontend/img/card.png');?>'">
    </div>
  <?php 
      endforeach;
    endif;
  ?>

  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
</div>