<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Banners extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('banners_m');
    }
    
    public function index()
    {
        $data['info'] = $this->banners_m->get_banners_by_type('home')->result();
        $this->load->view('banners',  $data); 
    }

    public function about()
    {
        $data['info'] = $this->banners_m->get_banners_by_type('about-us')->result();
        $this->load->view('banners',  $data); 
    }

    public function contact()
    {
        $data['info'] = $this->banners_m->get_banners_by_type('contact')->result();
        $this->load->view('banners',  $data); 
    }

    public function services()
    {
        $data['info'] = $this->banners_m->get_banners_by_type('services')->result();
        $this->load->view('banners',  $data); 
    }

    public function activities()
    {
        $data['info'] = $this->banners_m->get_banners_by_type('activities')->result();
        $this->load->view('banners',  $data); 
    }

    // public function products()
    // {
    //     $data['info'] = $this->banners_m->get_banners_by_type('Products and services')->result();
    //     $this->load->view('banners',  $data); 
    // }

    
}
