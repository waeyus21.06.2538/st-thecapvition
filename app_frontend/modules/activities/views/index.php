<div class="container-fluid">
    <div class="row">
        <div id="slide_banner" class="owl-carousel owl-theme">
            <?php
            if(isset($banners) && count($banners) > 0):
                foreach($banners as $banner):
            ?>
                <div class="item">
                    <img src="<?=base_url().$banner->file?>" class="ImgFluid" alt="<?=$banner->title?>">
                </div>
            <?php
                endforeach;
            endif;
            ?>
            
        </div>
    </div>
</div>