<?php  echo Modules::run('banners/activities',''); ?>
<div class="main main-raised">
  <div class="container">
    <div class="section text-center">
      <main>
        <div class="container ">
          <section>
            <div class="row mt-5">
              <div class="col-md-12 col-sm-12">
                <h1><?=$activities->title;?></h1>
                <img
                src="<?=base_url($activities->file)?>"
                class="img-fluid mt-3"
                style="width: 100%"
                onerror="this.src='<?php echo base_url('template/frontend/img/card.png');?>'"
                />
                <div class="mt-4 mb-4">
                  <strong>รายละเอียด :</strong>
                  <div class="sharethis-inline-share-buttons"></div>
                </div>

                <div><?=html_entity_decode($activities->excerpt)?></div>
              </div>
            </div>
          </section>
        </div>
      </main>
      <!--Main layout-->
    </div>
  </div>
</div>

