<?php  echo Modules::run('banners/activities',''); ?>
<div class="main main-raised">
  <div class="container">
    <div class="section text-center">
      <div class="container">
        <div class="row mt-5">
          <?php 
          if(!empty($info)):
            foreach($info as  $item):
              ?>
              <div class="col-md-4">
                <div class="card mb-4 shadow-sm">
                  <div class="">
                    <img src="<?=base_url($item->file)?>" class="card-img-top" alt="<?=$item->title? $item->title : ''?>">
                    <div class="card-body text-center">
                      <p class="card-text"><?=$item->title? $item->title : ''?></p>
                      <a href="<?=$item->title? base_url().'activities/detail/'.$item->slug: ''?>" class="btn btn-primary stretched-link">รายละเอียด</a>
                    </div>
                  </div>
                </div>
              </div>
              <?php 
            endforeach;
          endif;
          ?>
        </div>
      </div>

    </div>
  </div>
</div>

