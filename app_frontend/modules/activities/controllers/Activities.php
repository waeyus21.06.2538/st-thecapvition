<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activities extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('activities_m');
    }
    
    private function seo(){
        $obj_seo = $this->activities_m->get_activities_all()->row();
        $title          = 'TheCapvition | Activities';
        $robots         = config_item('siteTitle');
        $description    = config_item('metaDescription');
        $keywords       = config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('template/frontend/img/banner/mobile.jpg');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    
    }

    private function seo_detail($slug){

		$obj_seo  = $this->activities_m->get_activities_by_slug($slug)->row();
        $title          = !empty($obj_seo->metaTitle)? config_item('siteTitle').' | '.$obj_seo->metaTitle : config_item('siteTitle').' | Services';
        $robots         = !empty($obj_seo->metaTitle)? $obj_seo->metaTitle : config_item('siteTitle');
        $description    = !empty($obj_seo->metaDescription)? $obj_seo->metaDescription : config_item('metaDescription');
        $keywords       = !empty($obj_seo->metaKeyword)? $obj_seo->metaKeyword : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('template/frontend/img/banner/mobile.jpg');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('articles/detail/'.$slug)."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

    public function index()
    {
        $data = array(
            'menu'          => 'activities',
            'seo'           => $this->seo(),
            'navbar'        => 'navbar',
            'content'       => 'activities-data',
            'footer'        => 'footer',
            'function'      =>  array('blogs'),
        );
        $data['info'] = $this->activities_m->get_activities_all()->result(); 
        $this->load->view('template/body', $data);
    }

    public function home()
    {
        $input['length'] = 6;
        $input['start'] = 0;
        $data['info'] = $this->activities_m->get_activities_option_all($input)->result();
        $this->load->view('activities',  $data); 
    }

    public function detail($slug ='')
    {
        $data = array(
            'menu'          => 'activities',
            'seo'           => $this->seo_detail($slug),
            'navbar'        => 'navbar',
            'content'       => 'activities-detail',
            'footer'        => 'footer',
            'function'      =>  array('blogs'),
        );
        $data['activities'] = $this->activities_m->get_activities_by_slug($slug)->row();        
        $this->load->view('template/body', $data);
    }
    
}
