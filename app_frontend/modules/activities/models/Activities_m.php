<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Activities_m extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        // Your own constructor code
    }
    
    // public function get_rows($param){
    //     if ( isset($param['length']) ) 
    //         $this->db->limit($param['length'], $param['start']);
        
    //     if ( isset($param['banner_id']) ) 
    //         $this->db->where('a.banner_id', $param['banner_id']);

    //     if ( isset($param['recycle']) )
    //         $this->db->where('a.recycle', $param['recycle']);
            
    //     if ( isset($param['active']) )
    //         $this->db->where('a.active', $param['active']);

    //     if ( isset($param['type']) )
    //         $this->db->where('a.type', $param['type']);
             
    //     $query = $this->db
    //                     ->select('a.*')
    //                     ->from('banners a')
    //                     ->get();
    //     return $query;
    // }

    // new function query
    public function count_activities_all() 
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        return $this->db->count_all_results('activities');
    }

    public function get_activities_all()
    {
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('activities');
        return $query;
    }

    public function get_activities_by_id($id)
    {
        $this->db->where('services_id', $id);
        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('activities');
        
        return $query;
    }

    
    public function get_activities_by_slug($slug)
    {
        $query = $this->db
        ->where('slug', $slug)
        ->set('qty_eye','qty_eye+1',FALSE)
        ->update('activities');

       $query = $this->db
        ->select('a.*')
        ->from('activities a')
        ->where('a.slug', $slug)
        ->get();

        return $query;
    }

    public function get_activities_option_all($param)
    {
        if (!empty($param['length'])):
            $this->db->limit($param['length'], $param['start']);
        endif;

        if (!empty($param['type'])):
            $this->db->where('type', $param['type']);
        endif;

        $this->db->where('recycle', 0);
        $this->db->where('active', 1);
        $this->db->order_by('created_at', 'DESC');
        $this->db->select('*');
        $query = $this->db->get('activities');
        
        return $query;
    }
}
