<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends MX_Controller {

	function __construct() {
		parent::__construct();
    }

    private function seo(){

        $title          = 'TheCapvition | Contact Us';
        $robots         = config_item('siteTitle');
        $description    = config_item('metaDescription');
        $keywords       = config_item('metaKeyword');
        $img            = site_url('template/frontend/img/banner/mobile.jpg');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='website'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    
    }
    
    public function index(){
        $query = $this->db
                        ->select('a.*')
                        ->from('table_all a')
                        ->where('a.type', 'contact')
                        ->get()->row();
		
		//Modules::run('track/front','');
        $data = array(
            'menu'    => 'contact',
            'seo'     => $this->seo(),
            'navbar'  => 'navbar',
            'content' => 'contact',
            'footer'  => 'footer',
            //'function'=>  array('custom','home'),
        );
        $data['info'] = $query;

        $this->load->view('template/body', $data);
    }

    public function mail()
    {
        if(isset( $_POST['name']))

            $name = $_POST['name'];

        if(isset( $_POST['email']))

            $email = $_POST['email'];

        if(isset( $_POST['message']))

            $message = $_POST['message'];
        
        if(isset( $_POST['tel']))

            $message = $_POST['tel'];

        if ($name === ''){
            echo "Name cannot be empty.";
            die();
        }

        if ($email === ''){
            echo "Email cannot be empty.";
            die();
        } else {

            if (!filter_var($email, FILTER_VALIDATE_EMAIL)){
                echo "Email format invalid.";
                die();
            }
        }

        if ($message === ''){
            echo "Message cannot be empty.";
            die();
        }

        $subject = 'ติดต่อสอบถาม';

        $content="From: $name \nEmail: $email \nMessage: $message";

        $recipient = "suphitsara.ta@yahoo.com";
        //$recipient = "zun7441.yru@gmail.com";


        $mailheader = "From: $email \r\n";

        mail($recipient, $subject, $content, $mailheader) or die("Error!");

        $data = array(
            'menu'    => 'contact',
            'seo'     => $this->seo(),
            'navbar'  => 'navbar',
            'content' => 'sent',
            'footer'  => 'footer',
            //'function'=>  array('custom','home'),
        );

        $data['send'] = "ส่งข้อมูลเรียบร้อย !";

        $this->load->view('template/body', $data);

    }

}