<!-- Full Page Intro -->
<?php  echo Modules::run('banners/contact',''); ?>
<!-- Full Page Intro -->

<!--Main layout-->
<main>
  <div class="container">
    <!--Section: Main info-->
    <section class="mt-5-not-banner ">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <section class="m-3 card-contact">
            <form id="contact-form" name="contact-form" action="<?=base_url('contact/mail')?>" method="POST">
              <div class="card">
                <div class="card-body">
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Name</label>
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="name" required />
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="inputPassword" class="col-sm-2 col-form-label">Email</label>
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <input type="email" class="form-control" name="email" required />
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Tel</label>
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <input type="text" class="form-control" name="tel" required />
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-2 col-form-label">Detail</label>
                    <div class="col-sm-10">
                      <div class="col-sm-10">
                        <textarea name="message" rows="5" class="form-control" required></textarea>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-sm-12 text-center">
                      <button class="btn btn-primary mo " type="submit">
                        Send
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </form>
          </section>
        </div>
        <div class="col-md-6 col-sm-6">
          <section class="m-3">
            <?=!empty($info->excerpt)? html_entity_decode($info->excerpt) : ''?>
          </section>
        </div>
      </div>
    </section>
    <!--Section: Main info-->
  </div>
</main>
<!--Main layout-->