<!--Main layout-->
<main>
  <div class="container">
    <!--Section: Main info-->
    <section class="mt-5-not-banner ">
      <div class="row">
        <div class="col-md-6 col-sm-6">
          <div class="alert alert-success text-center">
            <h5 class="">ส่งเมลเรียบร้อย !!</h5>
            <h5 class="mt-5">ขอบคุณค่ะ</h5>
          </div>
        </div>
        <div class="col-md-6 col-sm-6">
          <img
            class="img-fluid"
            src="<?=base_url('template/frontend/')?>/img/content/plang05.png"
            alt=""
          />
        </div>
      </div>
    </section>
    <!--Section: Main info-->
  </div>
</main>
<!--Main layout-->
