<input type="hidden" name="num_row" id="num_row" value="<?=$num_row?>">
<input type="hidden" name="prepage" id="prepage" value="<?=$per_page?>">
<?php foreach($blogs as $bog):?>
<div class="col-md-4 mb-4">
    <a href="<?=base_url('blogs/detail/'.$bog->slug)?>">
    <div class="card">
    <img
        class="card-img-top"
        src="<?=base_url($bog->file);?>"
        alt="image"
        style="width:100%" onerror="this.src='<?php echo base_url('template/frontend/img/card.png');?>'"/>
    <div class="card-body">
        <?php if(isset($bog->article_categorie_id)):?>
        <div class="box-card-category mb-4"><?=$bog->c_title?></div>
    <?php endif;?>
        <h4 class="card-title name"><?=$bog->title?></h4>
        <p class="card-text">
        <?=$bog->excerpt?>
        </p>
        <p class="p-date"><?=DateEng($bog->updated_at);?></p>
    </div>
    </div>
    </a>  
</div>
<?php endforeach;?>