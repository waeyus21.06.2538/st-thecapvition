<div class="main main-raised">
  <div class="container">
    <div class="section text-center">
      <!--Main layout-->
      <main>
        <div class="container ">
          <!--Section: Main info-->
          <section class="mt-5-not-banner wow fadeIn">
            <div class="row">
              <div class="col-md-2 col-sm-6 col text-center">
               <a href="<?=base_url('blogs').'#focus'?>">
                <img class="mb-1" src="<?=base_url('template/frontend/img/icon/group00.png');?>" alt="" />
                <p>All</p>
              </a>
            </div>
            <?php foreach($categories as $cat):?>
              <div class="col-md-2 col-sm-6 col text-center">
                <a href="<?=base_url('blogs').'?category='.$cat->slug.'#focus'?>">
                  <img class="mb-1" src="<?=base_url($cat->file)?>" alt="" />
                  <p><?=$cat->title;?></p>
                </a>
              </div>
            <?php endforeach; ?>
          </div>
        </section>
        <!--Section: Main info-->
        <?php if(sizeof($blogs)):?> 
          <section id="focus">
            <div class="row mt-5" id="post-data">
              <?php
              $this->load->view('blog-data', $posts);
              ?>
            </div>  
            <!--Grid row place holder-->
            <?php $this->load->view('bolg-place-holder'); ?> 
            <!--End Grid row place holder-->  
          </section>
          <?php else:?> 
           <section id="focus">
             <hr>
             <div class="mt-5 ">
              <div class="card">
                <div class="card-body">
                  <div class="alert alert-warning text-center">
                    <strong>ขออภัยค่ะ !</strong> ไม่มีข้อมูลในหมวดนี้
                  </div>
                </div>
              </div>
              
              
            </div>
          </section>
        <?php endif; ?> 
      </div>
    </main>
    <!--Main layout-->

  </div>
</div>
</div>
