<?php if(sizeof($blogs)):?>
<section>
  <div class="row mt-5">
<?php foreach($blogs as $bog):?>
  
    <div class="col-md-4 mb-4">
      <a href="<?=base_url('blogs/detail/'.$bog->slug)?>">
      <div class="card">
        <img
          class="card-img-top img-fluid"
          src="<?=base_url($bog->file);?>"
          alt="image"
          onerror="this.src='<?php echo base_url('template/frontend/img/card.png');?>'"/>
        <div class="card-body">
          <?php if(isset($bog->article_categorie_id)):?>
          <div class="box-card-category mb-4"><?=$bog->c_title?></div>
         <?php endif;?>
          <h4 class="card-title name"><?=$bog->title?></h4>
          <p class="card-text">
            <?=$bog->excerpt?>
          </p>
          <p class="p-date"><?=DateEng($bog->updated_at);?></p>
        </div>
      </div>
      </a>  
    </div>
   

<?php endforeach;?>
  </div>
  <!--Grid row-->
</section>
<?php endif; ?>