<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blogs extends MX_Controller {
    
    private $perPage = 3;

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('blogs/blog_m');
    }

    private function seo($data = ''){

        $title          = isset($data['title']) ?  config_item('siteTitle').' | '.$data['title'] : config_item('siteTitle').' | Products and services';
        $robots         = config_item('siteTitle');
        $description    = isset($data['description']) ?  $data['description'] : config_item('metaDescription');
        $keywords       = isset($data['keywords']) ?  $data['keywords'] : config_item('metaKeyword');
        $img            = isset($data['img']) ?  site_url($data['img']) :  site_url('template/frontend/img/banner/mobile.jpg');
        $url            = isset($data['url']) ?  $data['url'] :  site_url();
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".$url."'/>";
        $meta          .= "<meta property='og:type' content='website'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    
    }
    
    public function index()
    {
        $input['length'] = 6;
        $input['start'] = 0;
        $input['active'] =1;

        $data = array(
            'menu'          => 'blogs',
            'seo'           => $this->seo(),
            'navbar'        => 'navbar',
            'content'       => 'blogs',
            'footer'        => 'footer',
            'function'      =>  array('blogs'),
        );
        //get category
        $data['categories'] = $this->blog_m->get_category()->result();

        $input['category'] = $this->input->get("category");

        $data['num_row'] = $this->blog_m->get_num($input['category'])->num_rows();

        $data['per_page'] = $this->perPage;

        $seo['url'] = site_url('blogs');
        
        
        if(!empty($this->input->get("page"))){
           $input['start'] = ceil($this->input->get("page") * $this->perPage);
           $input['length'] = $this->perPage;
           $data['blogs'] = $this->blog_m->get_rows($input)->result();
    
           $result = $this->load->view('blog-data', $data);
           json_encode($result);  
        }else{    
           $data['blogs'] = $this->blog_m->get_rows($input)->result();
           $this->load->view('template/body', $data);
        } 
    }

    public function home()
    {
        $input['length'] = 6;
        $input['start'] = 0;
        $input['active'] =1;

        $data['blogs'] = $this->blog_m->get_rows($input)->result();

        $this->load->view('blog-home',  $data); 
    }

     public function detail($slug)
    {
         $data = array(
            'menu'          => 'blogs',
            'navbar'        => 'navbar',
            'content'       => 'blog-detail',
            'footer'        => 'footer',
            'function'      =>  array('blogs'),
        );
        $data['blogs'] = $this->blog_m->get_blog($slug)->row();
        //var_dump($data['blogs']);
        $data['categories'] = $this->blog_m->get_category()->result();

        $seo['title'] = $data['blogs']->metaTitle;
        $seo['description'] = $data['blogs']->metaDescription;
        $seo['keywords'] = $data['blogs']->metaKeyword;
        $seo['img'] = $data['blogs']->file;
        $seo['url'] = site_url('blogs/detail'.$data['blogs']->slug);

        $data['seo'] = $this->seo($seo);

        $this->load->view('template/body', $data);
    }

    public function search()
    {
        $data = array(
            'menu'          => 'blogs',
            'seo'           => $this->seo(),
            'navbar'        => 'navbar',
            'content'       => 'blog-search',
            'footer'        => 'footer',
            'function'      =>  array('blogs'),
        );
       $input['search'] = $this->input->get('p');
       $data['search'] = $input['search'];
       $input['active'] =1;
       $data['blogs'] = $this->blog_m->get_rows($input)->result();

       $this->load->view('template/body', $data);
    }
}
