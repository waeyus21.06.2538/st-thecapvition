<nav class="navbar navbar-color-on-scroll navbar-transparent fixed-top navbar-expand-lg" color-on-scroll="100">
  <div class="container">
    <div class="navbar-translate">
      <a class="navbar-brand" href="/">
        <img src="<?=base_url('template/material-kit-master/assets/img/logo.png');?>" alt="thecapvision">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="sr-only">Toggle navigation</span>
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
        <span class="navbar-toggler-icon"></span>
      </button>
    </div>
    <div class="collapse navbar-collapse">
      <ul class="navbar-nav ml-auto">
        <li class="dropdown nav-item">
          <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
            <i class="material-icons">apps</i> บริการ
          </a>
          <div class="dropdown-menu dropdown-with-icons">
            <?php 
              echo Modules::run('services/menu');
            ?>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=base_url('activities');?>" >
            <i class="material-icons">calendar_today</i> กิจกรรม
          </a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="<?=base_url('aboutus');?>" >
            <i class="material-icons">import_contacts</i> เกี่ยวกับเรา
          </a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="<?=base_url('contact');?>" >
            <i class="material-icons">contact_mail</i> ติดต่อเรา
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://twitter.com/CreativeTim" target="_blank" data-original-title="Follow us on Twitter">
            <i class="fa fa-twitter"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.facebook.com/CreativeTim" target="_blank" data-original-title="Like us on Facebook">
            <i class="fa fa-facebook-square"></i>
          </a>
        </li>
        <li class="nav-item">
          <a class="nav-link" rel="tooltip" title="" data-placement="bottom" href="https://www.instagram.com/CreativeTimOfficial" target="_blank" data-original-title="Follow us on Instagram">
            <i class="fa fa-instagram"></i>
          </a>
        </li>
      </ul>
    </div>
    <div id="navbar-menu" class="collapse mt-3">
       <ul class="navbar-nav ml-auto">
        <li class="dropdown nav-item">
          <a href="#" class="dropdown-toggle nav-link" data-toggle="dropdown">
            <i class="material-icons">apps</i> บริการ
          </a>
          <div class="dropdown-menu dropdown-with-icons">
            <?php 
              echo Modules::run('services/menu');
            ?>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?=base_url('activities');?>" >
            <i class="material-icons">calendar_today</i> กิจกรรม
          </a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="<?=base_url('aboutus');?>" >
            <i class="material-icons">import_contacts</i> เกี่ยวกับเรา
          </a>
        </li>
         <li class="nav-item">
          <a class="nav-link" href="<?=base_url('contact');?>" >
            <i class="material-icons">contact_mail</i> ติดต่อเรา
          </a>
        </li>
      </ul>
  </div>
  </div>
</nav>