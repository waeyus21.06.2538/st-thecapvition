<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta http-equiv="x-ua-compatible" content="ie=edge" />

    <meta property="fb:app_id"          content="1087926091264789" /> 
    <?=$seo?>
    <!-- Font Awesome -->
    <link
      rel="stylesheet"
      href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"
    />
    <!-- Bootstrap core CSS -->
    <link href="<?=base_url('template/frontend/bootstrap/css/bootstrap.min.css');?>" rel="stylesheet" />
    <!-- <link href="<?=base_url('template/frontend/css/bootstrap.min.css');?>" rel="stylesheet" /> -->
    <!-- Material Design Bootstrap -->
    <!-- <link href="<?=base_url('template/frontend/css/mdb.min.css');?>" rel="stylesheet" /> -->
    <!-- Your custom styles (optional) -->
    <!-- <link href="<?=base_url('template/frontend/css/style.min.css');?>" rel="stylesheet" /> -->

    <link rel="shortcut icon" href="<?=base_url('favicon.ico')?>" type="image/x-icon">
    <link rel="icon" href="<?=base_url('favicon.ico')?>" type="image/x-icon">

    <style type="text/css">
      /* @media (min-width: 800px) and (max-width: 850px) {
        .navbar:not(.top-nav-collapse) {
          background: #1c2331 !important;
        }
      } */

      .size-banner{
        max-height: 400px !important;
      }
      img.pc {
        display: block;
      }

      img.mobile {
        display: none;
      }

      @media (max-width: 576px){
        .size-banner{
          max-height: 480px !important;
        }
          img.pc {
              display: none;
          }

          img.mobile {
            display: block;
        }
      }
    </style>
    <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5cefdcc00180a5001283121f&product=inline-share-buttons" async="async"> </script>
    <div id="1087926091264789"></div>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.3&appId=186839091824818&autoLogAppEvents=1"></script>  

<script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1087926091264789',
      cookie     : true,
      xfbml      : true,
      version    : 'v3.3'
    });
      
    FB.AppEvents.logPageView();   
      
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>  
</head>

  <body>
		
    <?php $this->load->view($navbar,'');?>

		<?php $this->load->view($content);?>


    <!--Footer-->
     <?php $this->load->view($footer);?>
    <!--/.Footer-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="<?=base_url('template/frontend/js/jquery-3.4.0.min.js');?>"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="<?=base_url('template/frontend/js/popper.min.js');?>"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="<?=base_url('template/frontend/js/bootstrap.min.js');?>"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="<?=base_url('template/frontend/js/mdb.min.js');?>"></script>
    <!-- Initializations -->
    <script type="text/javascript">
      // Animations initialization
      new WOW().init();
    </script>
    	<?php
		if(!empty($function)){
			$length2 = count($function);
			for ($x = 0; $x < $length2; $x++) {
				$this->load->view('function/'.$function[$x].'/script');
			}
		}
		?>
  </body>
</html>

<script>
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if (scroll >= 10) {
            $('#myNavbar').addClass('fixed-top');
        } else {
            $('#myNavbar').removeClass('fixed-top');
        }
    });
</script>
