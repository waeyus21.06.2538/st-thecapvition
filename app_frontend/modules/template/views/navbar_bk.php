<header>
  <!-- myNavbar fixed-top -->
  <nav id="myNavbar" class="navbar navbar-expand-md navbar-dark bg-dark">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03"
      aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a class="navbar-brand" href="#">Navbar</a>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item <?=$menu == 'home' ? 'active' : '';?>">
          <a class="nav-link" href="<?=base_url('home');?>">หน้าหลัก</a>
        </li>
        <li class="nav-item dropdown <?=$menu == 'services' ? 'active' : '';?>">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
            aria-haspopup="true" aria-expanded="false">
            บริการ
          </a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <?php 
              echo Modules::run('services/menu');
            ?>
            <!-- <a class="dropdown-item" href="#">คูปองพัฒนาครู2562</a>
            <a class="dropdown-item" href="#">คูปองพัฒนาครู2563</a> -->
          </div>
        </li>
        <li class="nav-item <?=$menu == 'activities' ? 'active' : '';?>">
          <a class="nav-link" href="<?=base_url('activities');?>">กิจกรรม</a>
        </li>
        <li class="nav-item <?=$menu == 'aboutus' ? 'active' : '';?>">
          <a class="nav-link" href="<?=base_url('aboutus');?>">เกี่ยวกับเรา</a>
        </li>
        <li class="nav-item <?=$menu == 'contact' ? 'active' : '';?>">
          <a class="nav-link" href="<?=base_url('contact');?>">ติดต่อเรา</a>
        </li>
      </ul>
      <!-- <form class="form-inline my-2 my-lg-0">
        <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
      </form> -->
    </div>
  </nav>
</header>