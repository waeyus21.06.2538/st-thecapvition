<!DOCTYPE html>
<html lang="en">

<head>
  <title>Hello, world!</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <!--     Fonts and icons     -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
  <!-- Material Kit CSS -->
  <!-- CSS Files -->
  <link href="<?=base_url('template/material-kit-master/assets/css/material-kit.css?v=2.0.5');?>" rel="stylesheet" />
  <!-- CSS Just for demo purpose, don't include it in your project -->
  <link href="<?=base_url('template/material-kit-master/assets/demo/demo.css');?>" rel="stylesheet" />

  <link href="<?=base_url('template/material-kit-master/assets/css/custom.css');?>" rel="stylesheet" />

  <link href="https://fonts.googleapis.com/css?family=Prompt&display=swap" rel="stylesheet">

  <link rel="shortcut icon" href="<?=base_url('favicon.ico')?>" type="image/x-icon">
  <link rel="icon" href="<?=base_url('favicon.ico')?>" type="image/x-icon">

  <meta property="fb:app_id"          content="1087926091264789" /> 

  <?=$seo?>

  <script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=5cefdcc00180a5001283121f&product=inline-share-buttons" async="async"> </script>
  <div id="1087926091264789"></div>
  <script async defer crossorigin="anonymous" src="https://connect.facebook.net/th_TH/sdk.js#xfbml=1&version=v3.3&appId=186839091824818&autoLogAppEvents=1"></script>  

  <script>
    window.fbAsyncInit = function() {
      FB.init({
        appId      : '1087926091264789',
        cookie     : true,
        xfbml      : true,
        version    : 'v3.3'
      });
      
      FB.AppEvents.logPageView();   
      
    };

    (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
 </script>

</head>

<body>

  <?php $this->load->view($navbar,'');?>

  <?php $this->load->view($content);?>


  <!--Footer-->
  <?php $this->load->view($footer);?>
  <!--/.Footer-->

  <!--   Core JS Files   -->
  <script src="<?=base_url('template/material-kit-master/assets/js/core/jquery.min.js')?>" type="text/javascript"></script>
  <script src="<?=base_url('template/material-kit-master/assets/js/core/popper.min.js')?>" type="text/javascript"></script>
  <script src="<?=base_url('template/material-kit-master/assets/js/core/bootstrap-material-design.min.js')?>" type="text/javascript"></script>
  <script src="<?=base_url('template/material-kit-master/assets/js/plugins/moment.min.js')?>"></script>
  <!--  Plugin for the Datepicker, full documentation here: https://github.com/Eonasdan/bootstrap-datetimepicker -->
  <script src="<?=base_url('template/material-kit-master/assets/js/plugins/bootstrap-datetimepicker.js')?>" type="text/javascript"></script>
  <!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
  <script src="<?=base_url('template/material-kit-master/assets/js/plugins/nouislider.min.js')?>" type="text/javascript"></script>


  <script>

    function scrollToDownload() {
      if ($('.section-download').length != 0) {
        $("html, body").animate({
          scrollTop: $('.section-download').offset().top
        }, 1000);
      }
    }

    $(document).ready(function () {

      $('.navbar-toggler').click(function(event) {
        $('.navbar.navbar-transparent').attr('style', 'background: #fff !important');
        $('.navbar.navbar-transparent .navbar-toggler .navbar-toggler-icon').css({
         'background-color': '#000'
       });
      });
    
      $(window).bind('scroll', function() {
       var navHeight = $( window ).height() - 200;
       if ($(window).scrollTop() > navHeight) {
        $('.navbar.navbar-transparent').attr('style', 'background: #fff !important');
        $('.navbar.navbar-transparent .navbar-toggler .navbar-toggler-icon').css({
         'background-color': '#000'
       });
        $('.navbar.navbar-transparent').css({
          color: '#000',
        });
      }else{
        if($('#navbar-menu').hasClass('show')){

        }else{
         $('.navbar.navbar-transparent').attr('style', 'background: transparent !important');
         $('.navbar.navbar-transparent .navbar-toggler .navbar-toggler-icon').css({
           'background-color': '#fff'
         }); 
       }  

     }
   });


    });

  </script>

</body>

</html>