<script type="text/javascript">

	var page = 1;
	$(window).scroll(function() {
		
	    // if($(window).scrollTop() + $(window).height() >= $(document).height()) {
	    //     page++;
	    //     loadMoreData(page);
	    // }
		scrollDistance = $(window).scrollTop() + $(window).height();
		footerDistance = $('footer').offset().top;

		if (scrollDistance >= footerDistance) {
            var  num_row = $("#num_row").val();
			var prepage= $("#prepage").val();
			var url = '?page=';
			page++;
			if(page <= Math.ceil(num_row/prepage) ){
			   url = url + page;	
			   loadMoreData(page,url);
			}   
		}
	});


	function loadMoreData(page,url){
	 
	  $.ajax(
	        {
	            url: url,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
				setTimeout(function(){
					$('.ajax-load').hide();
					$("#post-data").append(data);
				}, 1000);
	            
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}

	
</script>
