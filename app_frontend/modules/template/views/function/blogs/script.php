<script type="text/javascript">
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
   };

	var page = 1;
	$(window).scroll(function() {
		
	    // if($(window).scrollTop() + $(window).height() >= $(document).height()) {
	    //     page++;
	    //     loadMoreData(page);
	    // }
		scrollDistance = $(window).scrollTop() + $(window).height();
		footerDistance = $('footer').offset().top;

		if (scrollDistance >= footerDistance) {
            var  num_row = $("#num_row").val();
			var prepage= $("#prepage").val();
			var category = getUrlParameter('category');
			var url = '?page=';
			page++;
			if(page <= Math.ceil(num_row/prepage) ){
				if(typeof category != "undefined"){
                  url = '?page=' + page +'&category=' + category;
				}else{
				  url = url + page;	
				}
			   loadMoreData(page,url);
			}   
		}
	});


	function loadMoreData(page,url){
	 
	  $.ajax(
	        {
	            url: url,
	            type: "get",
	            beforeSend: function()
	            {
	                $('.ajax-load').show();
	            }
	        })
	        .done(function(data)
	        {
	            if(data == " "){
	                $('.ajax-load').html("No more records found");
	                return;
	            }
				setTimeout(function(){
					$('.ajax-load').hide();
					$("#post-data").append(data);
				}, 1000);
	            
	        })
	        .fail(function(jqXHR, ajaxOptions, thrownError)
	        {
	              alert('server not responding...');
	        });
	}

	
</script>
