<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends MX_Controller {
    
    public function __construct() 
    {
        parent::__construct();
        $this->load->model('services_m');
    }
    
    private function seo_detail($slug){

		$obj_seo  = $this->services_m->get_services_by_slug($slug)->row();
        $title          = !empty($obj_seo->metaTitle)? config_item('siteTitle').' | '.$obj_seo->metaTitle : config_item('siteTitle').' | Services';
        $robots         = !empty($obj_seo->metaTitle)? $obj_seo->metaTitle : config_item('siteTitle');
        $description    = !empty($obj_seo->metaDescription)? $obj_seo->metaDescription : config_item('metaDescription');
        $keywords       = !empty($obj_seo->metaKeyword)? $obj_seo->metaKeyword : config_item('metaKeyword');
        $img            = !empty($obj_seo->file)? site_url($obj_seo->file) : site_url('template/frontend/img/banner/mobile.jpg');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url('articles/detail/'.$slug)."'/>";
        $meta          .= "<meta property='og:type' content='web'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }
    
    public function index()
    {
        $data['info'] = $this->services_m->get_services_all()->result();
        $this->load->view('service',  $data); 
    }

    public function menu()
    {
        $html = '';
        $data = $this->services_m->get_services_all()->result();
        if(!empty($data )):
            foreach($data as $item):
                $html.='<a class="dropdown-item" href="'.base_url().'services/detail/'.$item->slug.'">'.$item->title.'</a>';
            endforeach;
        endif;

        echo $html; 
    }

    public function detail($slug='')
    {
        $data = array(
            'menu'          => 'services',
            'seo'           => $this->seo_detail($slug),
            'navbar'        => 'navbar',
            'content'       => 'services-detail',
            'footer'        => 'footer',
            'function'      =>  array('blogs'),
        );

        $data['services'] = $this->services_m->get_services_by_slug($slug)->row();

        // arr($data['services']);
        // exit();
        // $seo['title'] = $data['products']->metaTitle;
        // $seo['description'] = $data['products']->metaDescription;
        // $seo['keywords'] = $data['products']->metaKeyword;
        // $seo['img'] = $data['products']->file;
        // $seo['url'] = site_url('products/detail'.$data['blogs']->slug);

        // $data['seo'] = $this->seo($seo);
        
        $this->load->view('template/body', $data);

    }
    
}
