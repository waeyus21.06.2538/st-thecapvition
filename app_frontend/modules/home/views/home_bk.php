    <!-- Full Page Intro -->
    <?php  echo Modules::run('banners/index',''); ?>
    <!-- Full Page Intro -->

    <!--Main layout-->
    <main role="main">
      <section class="mt-5 text-center">
        <div class="container">
          <h1 class="jumbotron-heading">บริการ</h1>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
            <?php  echo Modules::run('services/index',''); ?>
            <!-- <div class="col-md-4">
              <div class="card mb-4 shadow-sm">
                <div class="card">
                  <img src="http://st-thecapvition.test:82/uploads/banners/2019/07/4ec2d260eccc1bf71dc3b8670406708c.jpg" class="card-img-top" alt="...">
                  <div class="card-body">
                    <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
              </div>
            </div> -->

          </div>
        </div>
      </div>

      <section class="mt-5 text-center">
        <div class="container">
          <h1 class="jumbotron-heading">กิจกรรม</h1>
      </section>

      <div class="album py-5 bg-light">
        <div class="container">

          <div class="row">
            <?php  echo Modules::run('activities/home',''); ?>
          </div>
        </div>
      </div>



      <?php  //echo Modules::run('blogs/home',''); ?>
    </main>
    <!--Main layout-->