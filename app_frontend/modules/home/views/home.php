    <!-- Full Page Intro -->
    <?php  echo Modules::run('banners/index',''); ?>
    <!-- Full Page Intro -->
    <div class="main main-raised">
      <div class="container">
        <div class="section text-center">
          <!--Main layout-->
          <main role="main">
            <section class="mt-5 text-center">
              <div class="container">
                <h1 class="jumbotron-heading">บริการ</h1>
              </div>
            </section>

            <div class="album py-5 bg-light">
              <div class="container">

                <div class="row">
                  <?php  echo Modules::run('services/index',''); ?>
                </div>
              </div>
            </div>

            <section class="mt-5 text-center">
              <div class="container">
                <h1 class="jumbotron-heading">กิจกรรม</h1>
              </section>

              <div class="album py-5 bg-light">
                <div class="container">

                  <div class="row">
                    <?php  echo Modules::run('activities/home',''); ?>
                  </div>
                </div>
              </div>



              <?php  //echo Modules::run('blogs/home',''); ?>
            </main>
            <!--Main layout-->
          </div>
        </div>
      </div>