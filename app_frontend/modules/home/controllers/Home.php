<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MX_Controller {

	function __construct() {
		parent::__construct();
        $this->load->model('blogs/blog_m');
	}

	 private function seo(){

        $title          = config_item('siteTitle');
        $robots         = config_item('siteTitle');
        $description    = config_item('metaDescription');
        $keywords       = config_item('metaKeyword');
        $img            = site_url('template/frontend/img/banner/mobile.jpg');
        $meta           = "<TITLE>".$title."</TITLE>";
        $meta          .= "<META name='robots' content='".$robots."'/>";
        $meta          .= "<META name='description' content='".$description."'/>";
        $meta          .= "<META name='keywords' content='".$keywords."'/>";
        $meta          .= "<meta property='og:url' content='".site_url()."'/>";
        $meta          .= "<meta property='og:type' content='website'/>";
        $meta          .= "<meta property='og:title' content='".$title."'/>";
        $meta          .= "<meta property='og:description' content='".$description."'/>";
        $meta          .= "<meta property='og:image' content='".$img."'/>";
        return $meta;
    }

	public function index(){
		
		Modules::run('track/front','');
        $data = array(
            'menu'    => 'home',
            'seo'     => $this->seo(),
            'navbar'  => 'navbar',
            'content' => 'home',
            'footer'  => 'footer',
            //'function'=>  array('custom','home'),
        );
        
        $data['categories'] = $this->blog_m->get_category()->result();

        $this->load->view('template/body', $data);
    }
    
    public function check_instructer() 
	{
        $instructors = $this->db->get_where('instructors', array('user_id' => $this->session->users['UID']));
		return $instructors;
    }
}
